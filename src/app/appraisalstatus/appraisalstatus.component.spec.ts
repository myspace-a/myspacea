import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AppraisalstatusComponent } from './appraisalstatus.component';

describe('AppraisalstatusComponent', () => {
  let component: AppraisalstatusComponent;
  let fixture: ComponentFixture<AppraisalstatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AppraisalstatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppraisalstatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
