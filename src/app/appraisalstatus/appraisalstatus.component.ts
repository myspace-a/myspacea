import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-appraisalstatus',
  templateUrl: './appraisalstatus.component.html',
  styleUrls: ['./appraisalstatus.component.css']
})
//In this we are getting appraisal info from appraisalstatus html  page  and collection info through ViewChild  directive 
export class AppraisalstatusComponent implements OnInit {
  @ViewChild('f') loginform:NgForm;
  constructor(private service:ServiceService ) { }

  ngOnInit() {

  }
  //here we collect info from form  and sends to api  and receives info from service whether appraisalstaus has accepted or already accepted
  onSubmit()
  {
    debugger;
    console.log("data")
    this.service.getAppraisalStatus(this.loginform.value).subscribe(
      (data)=>{
        console.log(data)
        if(data.statuscode == 200)
        {
         alert("your request is accepted");
         
        }
        else{
          alert("your request is already requested");
        }
        
      }
    )
          
    
  }

}
