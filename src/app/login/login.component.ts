import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ServiceService } from '../service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
/**
 * loginPage Component
 */
 /**
  * NGmodule declaration
  */
  @ViewChild('f') loginform:NgForm;
  constructor(private service:ServiceService , private router:Router) { }
 
  ngOnInit() {
  }
  /**
   * onSubmit Method to login with user credentials
   */
  onSubmit()
  {
  
    localStorage.setItem('name',this.loginform.value.username);
    this.service.doLogin(this.loginform.value).subscribe(
      (data)=>{
        if(data.statuscode == 200)
        {
          localStorage.setItem('jwt-token',data.token);
          localStorage.setItem("empId",data.empId); 
          this.router.navigate(['/notification'])
        }
        
      }
    )
          
    
  }


}
