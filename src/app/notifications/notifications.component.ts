import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.component.html',
  styleUrls: ['./notifications.component.css']
})
export class NotificationsComponent implements OnInit {
/**
 * 
 * @param service constructor to probvide service of http response
 */
  constructor(private service: ServiceService) { }
  /**
   * getNotification is method to get all notifications to the Employee
   */
  notifications;
  result: any;
  ngOnInit() {
    this.service.getNotification().subscribe((data: any) => {
      debugger;
      console.log(data);
      // alert(data);
      this.notifications = data;
      this.result = data.Notification;
      // alert(this.result.title)

    })
  }
}




