import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-rave',
  templateUrl: './rave.component.html',
  styleUrls: ['./rave.component.css']
})
export class RaveComponent implements OnInit {
  result:any;
  //here we are getting rave information from  service  method in ngoninit method and presenting that  in html 
  constructor(private service:ServiceService) { }
rave_data;
  ngOnInit() {
    this.service.getRaveInfo().subscribe((data:any)=>
    {
      debugger;
      console.log(data);
    
      this.rave_data=data;
      this.result= data.result;
      
    }) 
  }
 
}
