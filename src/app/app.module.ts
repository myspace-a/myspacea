import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule,routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import {FormsModule } from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { NavbarComponent } from './navbar/navbar.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { PostraveComponent } from './postrave/postrave.component';
import { AppraisalstatusComponent } from './appraisalstatus/appraisalstatus.component';
import { EmployeeinfoComponent } from './employeeinfo/employeeinfo.component';
import { TechsupportComponent } from './techsupport/techsupport.component';
import { SuccessfulprojectsComponent } from './successfulprojects/successfulprojects.component';
import { RaveComponent } from './rave/rave.component';

import { DriveComponent } from './drive/drive.component';
import { HolidaysComponent } from './holidays/holidays.component';

import { SearchComponent } from './search/search.component';

import { NotificationsComponent } from './notifications/notifications.component';
import { QuestionComponent } from './question/question.component';
import { AnswersComponent } from './answers/answers.component';



@NgModule({
  declarations: [
    AppComponent,
    routingComponents,
    NavbarComponent,
    HeaderComponent,
    FooterComponent,
    PostraveComponent,
    AppraisalstatusComponent,
    EmployeeinfoComponent,
    TechsupportComponent,
    SuccessfulprojectsComponent,
    RaveComponent,
    DriveComponent,
    HolidaysComponent,
    SearchComponent,
    NotificationsComponent,
    QuestionComponent,
    AnswersComponent,  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
  HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
