import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { HeaderComponent } from './header/header.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PostraveComponent } from './postrave/postrave.component';
import { AppraisalstatusComponent } from './appraisalstatus/appraisalstatus.component';
import { EmployeeinfoComponent } from './employeeinfo/employeeinfo.component';
import { TechsupportComponent } from './techsupport/techsupport.component';
import { SuccessfulprojectsComponent } from './successfulprojects/successfulprojects.component';
import { RaveComponent } from './rave/rave.component';
import { DriveComponent } from './drive/drive.component';
import { HolidaysComponent } from './holidays/holidays.component';
import { SearchComponent } from './search/search.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { QuestionComponent } from './question/question.component';
import { AnswersComponent } from './answers/answers.component';

const routes: Routes = [
  {path:'', component: LoginComponent },
  {path:'header', component: NavbarComponent},
  {path:'postrave', component: PostraveComponent},
  {path:'appraisalstatus', component:AppraisalstatusComponent},
  {path:'employeeInfo', component:EmployeeinfoComponent},
  {path:'techsupport', component:TechsupportComponent},
  {path:'rave', component:RaveComponent},
  {path:'holiday', component:HolidaysComponent},
  {path:'drive', component:DriveComponent},
  {path:'successfulprojects', component:SuccessfulprojectsComponent},
  {path:'search/:id', component:SearchComponent},
  {path:'notification', component:NotificationsComponent},
  {path:'question', component:QuestionComponent},
  {path:'answer/:id', component:AnswersComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const  routingComponents = [LoginComponent,NavbarComponent,PostraveComponent,AppraisalstatusComponent,EmployeeinfoComponent,TechsupportComponent,SuccessfulprojectsComponent,RaveComponent,NotificationsComponent,QuestionComponent]