import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private service:ServiceService,private router:Router ) { }
 
   empname :string = localStorage.getItem('name');
  ngOnInit() { 
  }
  /**
   * changeEvent Method for searchElement 
   */
  search:any;
  chaneEvent(event){
    this.search=event.target.value;
  }
  // searchEmployee Method  
  onSubmit(){
    debugger;
    console.log(this.search.charAt(0));

    // this.router.navigate(['/search',this.search])
    window.location.href="/search/"+this.search;
  
}
}
