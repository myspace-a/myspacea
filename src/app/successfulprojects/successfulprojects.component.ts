import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-successfulprojects',
  templateUrl: './successfulprojects.component.html',
  styleUrls: ['./successfulprojects.component.css']
})
//This contains info about successful projects 
export class SuccessfulprojectsComponent implements OnInit {

 array : any = [{ title: "iFusion", content: "This advanced analytics solution platform addresses data challenges by eliminating complexities in collecting data from heterogeneous sources and poly-structured data and combining the disparate sources through virtualization and federation. With its inbuilt rich set of algorithms and ability to onboard custom-built algorithms, iFusion Analytics enables enterprises to focus on building predictive and prescriptive analytics solutions instead of managing the Big Data “infrastructure stack” to achieve actionable insights." },
  { title: "Kiteboard", content: "A light-weight, small form factor and cost-effective Computer-on- Module (COM) Board for building Mobile Devices, Connected Embedded Systems and IoT Solutions." },
  { title: "Indra rugged tablet", content: " User-friendly heavyweight Performance Device based on Qualcomm quad-core processor, aimed for outdoor industrial use." },
  { title: "ARI tablet with iris scanner", content: " A Tablet Device with superior speed and performance that lets consumers multitask and helps users interact with data by providing quick access to frequently used apps, alerts and remote device management functionality. An Enterprise-grade Mobile Device with Biometric Authentication Systems (IRIS Scanner)." },
  ]
  constructor() { }

  ngOnInit() {
  }

}
