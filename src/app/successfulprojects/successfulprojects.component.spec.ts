import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuccessfulprojectsComponent } from './successfulprojects.component';

describe('SuccessfulprojectsComponent', () => {
  let component: SuccessfulprojectsComponent;
  let fixture: ComponentFixture<SuccessfulprojectsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuccessfulprojectsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuccessfulprojectsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
