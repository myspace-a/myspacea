import { Component, OnInit, ViewChild } from '@angular/core';
import { ServiceService } from '../service.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-employeeinfo',
  templateUrl: './employeeinfo.component.html',
  styleUrls: ['./employeeinfo.component.css']
})
//This is useful for getting employeeinfo
export class EmployeeinfoComponent implements OnInit {
 search:any;
  constructor(private service:ServiceService) { }
emp_data :any;
@ViewChild('f') loginform:NgForm;
//In this method we are getting  employee info from api calls
  ngOnInit() {
          
       this.service.getEmployeeInfo().subscribe((data:any)=>
        {
          debugger;
          console.log(data);
          if(data.statuscode == 200){
           
          }
          
         
          this.emp_data=data;
          console.log( this.emp_data);
        }) 
  }
//In this method we are getting updated info from user and it hits database and refresh page consists
onSubmit()
  {
    let id = localStorage.getItem('empId');
    console.log("submitted")
    this.loginform.value.employeeId = id ;
    console.log(this.loginform.value);
    this.service.Edit(this.loginform.value).subscribe(
      (data)=>{
        if(data.status == 200)
        {
          this.emp_data = data.updated;
          window.location.reload();
        }
        
      }
    )
          
    
  }
}
