import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.css']
})
export class QuestionComponent implements OnInit {

  constructor(private  service:ServiceService,private router:Router) { }
  // intilization of variables
 questions:any;
 question:any;
 answer:any;
 postquestionid:any;
 postquestion:{};
 postAnswer:{};
  id:string=localStorage.getItem("empId");
  
  ngOnInit() {
  // getallquestions method implementation for the postQusetions from the Employee
  this.service.getallquestions().subscribe((data:any)=>
  {
    debugger;
    console.log(data);
    
    if(data.statuscode == 200){
      debugger
      this.questions = data.Questions_list;
      console.log(this.questions);

    }
  })
}
/**
 * Submit Method for getting all Questions
 * @param questionid 
 * 
 * @param question 
 */
Submit(questionid:number,question:string)
{
  localStorage.setItem('questionid',question) 
  alert(questionid);
  this.router.navigate(['/answer',questionid])
}
//changeEvent for getting elements from the user
changeEvent(event){
  debugger;
  this.question = event.target.value;
console.log(this.question);
}
//changeAnswerEvent for getting elements from the user
changeAnswerEvent(event)
{
  this.answer = event.target.value;
}
delete(questionid)
{
  
  this.service.DeleteQuestion(questionid).subscribe((data:any)=>
  {
    debugger;
    console.log(data);
    
    if(data.statuscode == 200){
     window.location.reload();
    }
     
  //  console.log("question");

  })
  

}
/**
 * This mwthod is useful for posting question and sending question
 *  to sevice and getting status on hitting api
 */
onpost(){
  debugger;
  let option = window.confirm("Are you sure want to post the Question?"); 
  let id = localStorage.getItem('empId');
  // this.question.postedBy=id;
   this.postquestion={
    "postedBy":id,
    "question":this.question
   }
  if(option){
  
        debugger;
        this.service.PostQuestion(this.postquestion).subscribe((data:any)=>
        {
          debugger;
          console.log(data);
          if(data.statuscode == 200){
          window.location.reload();    
         }
        })     
  }
  else{
    this.question = '';
  }
}
/**
 * getting answerid on clicking viewanswer button 
 * @param id 
 */
answerId(id)
{
 this.postquestionid = id;
 console.log(id);
}
/**
 * This method is useful for posting the anwer and sending request service method and it returns status 
 */
Answerpost(){
  debugger;
  let option = window.confirm("Are you sure want to post the Answer?"); 
  let id = localStorage.getItem('empId');
  // this.question.postedBy=id;
   this.postAnswer={
    "postedBy":id,
    "answer":this.answer,
    "questionId":this.postquestionid
   }
  if(option){
        debugger;
        this.service.PostAnswer(this.postAnswer).subscribe((data:any)=>
        {
          debugger;
          console.log(data);
          
          if(data.statuscode == 200){
          window.location.reload();   
          }
        }) 
  }
  else{
    
  }
}
}
