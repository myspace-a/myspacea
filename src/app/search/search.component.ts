import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
/**
 * In this we get info search in from of parms and we get search info in this component
 */
export class SearchComponent implements OnInit {

  constructor(private service:ServiceService,private route:ActivatedRoute) { }
  emp_data;
  search:string;
  ngOnInit() {
        this.search = this.route.snapshot.paramMap.get('id');
    this.service.getSearch(this.search).subscribe((data:any)=>
        {
           console.log(data.result.name)
           this.emp_data=data; 
        })
 
  }
 

}
