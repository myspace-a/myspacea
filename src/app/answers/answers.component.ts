import { Component, OnInit } from '@angular/core';
import { ServiceService } from '../service.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-answers',
  templateUrl: './answers.component.html',
  styleUrls: ['./answers.component.css']
})
export class AnswersComponent implements OnInit {

  constructor(private service: ServiceService, private route: ActivatedRoute) { }
  answers;
  answer: string;
  questionid: string;
  question: string;
  EditAnswer: {};
  answerId: any;
  questionEditId: any;
  empId: any = localStorage.getItem('empId');
  //we are answers from service 
  ngOnInit() {
    this.questionid = this.route.snapshot.paramMap.get('id');
    this.service.getAllanswers(this.questionid).subscribe((data: any) => {
    console.log(data.result[0].answer)
    this.answers = data.result;

    })
    this.question = localStorage.getItem('questionid');
  }
 // In this we are getting answer and anserid and  questionEditId from button on clicking
  onSubmit(answer, answerId, questionEditId) {
    this.answer = answer;
    this.answerId = answerId;
    this.questionEditId = questionEditId;
  }
  //we will get  edit info infromation from edit form and sending that info to service
  editAnswer() {
    console.log(this.answer);
    this.EditAnswer = {
      "answerId": this.answerId,
      "postedBy": localStorage.getItem('empId'),
      "answer": this.answer,
      "questionId": this.questionEditId
    }
    let option = window.confirm("Are you sure want to post the Answer?");
    if (option) {
      debugger;
      this.service.OnEdit( this.EditAnswer).subscribe((data: any) => {
        debugger;
        console.log(data);

        if (data.statuscode == 200) {
          window.location.reload();
        }
      })
    }
    else {

    }
  }
 // we will get deletedId on clicking delete button and sending that info to service to delete  that answer
  deleteAnswer(deleteId)
  {
    let option = window.confirm("Are you sure want to delete the Answer?");
    if(option){
          debugger;
          this.service.deleteAnswer(deleteId).subscribe((data:any)=>
          {
            debugger;
            console.log(data);
            
            if(data.statuscode == 200){
            window.location.reload();   
            }
          })
        
     
    }
    else{      
    }
  }
}
