import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  
  constructor(private http: HttpClient ) {
   
  }
//This method gets login info and after hitting it to server it gets info and returns to subscribe method 
 doLogin(data:any)
 {
   return this.http.post<any>('http://192.168.150.64:8080/myspace/login',data);
 }

/**
 * It get edit profile info and hits api and returns status to subcribe method 
 * @param data 
 */
 Edit(data:any)
 {
  let token=localStorage.getItem("jwt-token");
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': token
    })
  };
   console.log("before data");
   return this.http.put<any>('http://192.168.150.64:8080/myspace/updateprofile',data,httpOptions);
  
 }
/**
 * It gets  appraisal info and hits the api and returns data to subscribe method
 * @param data 
 */
 getAppraisalStatus(data:any)
 {
   debugger;
   console.log(data);
  let token=localStorage.getItem("jwt-token");
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': token
    })
  };
 
    return   this.http.post<any>('http://192.168.150.64:8080/myspace/appraisal',data,httpOptions);   
 }
 /**
  * It gets search info and hits the api and get data based on name and employeeId 
  * @param search 
  */
 getSearch(search){
  let id=localStorage.getItem("empId");
  let token=localStorage.getItem("jwt-token");
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': token
    })
  };
console.log("displayed data");
console.log(search.charAt(0));
if(search.charAt(0)>= 1 && search.charAt(0) <= 9){
  console.log("searchbyid");
  return this.http.get<any>("http://192.168.150.64:8080/myspace/searchemployee/"+id+"/"+search,httpOptions
)  
}
else {
  console.log("searchbyname");
  return this.http.get<any>("http://192.168.150.64:8080/myspace/searchemployeebyname/"+id+"/"+search,httpOptions)
}
}
 /**
  * It gets employeeinfo based on employee id 
  */
getEmployeeInfo(){
  let token=localStorage.getItem("jwt-token");
  let id=localStorage.getItem("empId");
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': token
    })
  };
console.log("displayed data");
return this.http.get<any>("http://192.168.150.64:8080/myspace/profileview/"+id,httpOptions)
}
/**
 * we gets drive info from server and sends that info to subscribe method 
 */
getDriveInfo(){
  let token=localStorage.getItem("jwt-token");
  let id=localStorage.getItem("empId");
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': token
    })
  };
console.log(" drive displayed data");
return this.http.get<any>("http://192.168.150.64:8080/myspace/drive/"+id,httpOptions)
}
/**
 *we gets appraisal info from server and sends that info to subscribe method  
 */
getAppraisalInfo(){
  let token=localStorage.getItem("jwt-token");
  let id=localStorage.getItem("empId");
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': token
    })
  };
console.log("displayed data");
return this.http.get<any>("http://192.168.150.64:8080/myspace/appraisal/"+id,httpOptions)
}
/**
 *we gets rave info from server and sends that info to subscribe method  
 */

getRaveInfo(){
  let token=localStorage.getItem("jwt-token");
  let id=localStorage.getItem("empId");
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': token
    })
  };
console.log("displayed data");
return this.http.get<any>("http://192.168.150.64:8080/myspace/notifications/rave/all/"+id,httpOptions)
}
/**
 *It gets rave information from ravecomponent and sends that data to server and gets info after hitting on database and sends that info to method
 */
PostRave(data:any)
{
  debugger;
  console.log(data);
 let token=localStorage.getItem("jwt-token");
 const httpOptions = {
   headers: new HttpHeaders({
     'Content-Type':  'application/json',
     'Authorization': token
   })
 };

   return   this.http.post<any>('http://192.168.150.64:8080/myspace/notifications/rave/postnew',data,httpOptions);   
}
/**
 * It gets notification information  from database and sends that data to subscribe method 
 */
getNotification(){
  let token=localStorage.getItem("jwt-token");
  let id=localStorage.getItem("empId");
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': token
    })
  };
console.log("displayed data");
return this.http.get<any>("http://192.168.150.64:8080/myspace/latestnotification/"+id,httpOptions)
}
/**
 * It gets all questions from server and sends that info to subcribe method in question component 
 */
getallquestions(){
  let token=localStorage.getItem("jwt-token");
  let id=localStorage.getItem("empId");
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': token
    })
  };
  console.log("displayed data");
return this.http.get<any>("http://192.168.150.64:8080//myspace/queryforum/getallquestions/"+id,httpOptions)
}
/**
 * It gets all answers for a particular question from service and sends that data to getAllanswers component 
 * @param questionid 
 */
getAllanswers(questionid){
  let token=localStorage.getItem("jwt-token");
  let id=localStorage.getItem("empId");
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': token
    })
  };
  console.log("displayed data");
return this.http.get<any>("http://192.168.150.64:8080/myspace/queryforum/getallanswersforquestion/"+id+"/"+questionid, httpOptions)
}
/**
 * gets question info from component and sends that data to server and gets info from server 
 * @param data 
 */

PostQuestion(data:any)
{
  debugger;
  console.log(data);
  let id=localStorage.getItem("empId");
 let token=localStorage.getItem("jwt-token");
 const httpOptions = {
   headers: new HttpHeaders({
     'Content-Type':  'application/json',
     'Authorization': token
   })
 };

   return  this.http.post<any>('http://192.168.150.64:8080//myspace/queryforum/postquestion/'+id,data,httpOptions);   
}
/**
 * gets answerinformation from question component and hits the server and on hitting server gets the info from server
 * @param data 
 */
PostAnswer(data:any)
{
  debugger;
  console.log(data);
 let token=localStorage.getItem("jwt-token");
 const httpOptions = {
   headers: new HttpHeaders({
     'Content-Type':  'application/json',
     'Authorization': token
   })
 };
   return  this.http.post<any>('http://192.168.150.64:8080/myspace/queryforum/postnewanswer',data,httpOptions); 
}
/**
 * gets questionid to be deleted from server and hits the server and gets info and send that info to subscribe method
 * @param data 
 */
DeleteQuestion(data)
{
  debugger;
  let id=localStorage.getItem("empId");
 let token=localStorage.getItem("jwt-token");
 const httpOptions = {
   headers: new HttpHeaders({
     'Content-Type':  'application/json',
     'Authorization': token
   })
 };

   return  this.http.delete<any>('http://192.168.150.64:8080//myspace/queryforum/deletequestion/'+id+"/"+data,httpOptions);   
}
/**
 * It gets edit info from answer component and hits the server and gets info from server and sends that info to subscribe method 
 * @param data 
 */
OnEdit(data:any)
{
 let token=localStorage.getItem("jwt-token");
 const httpOptions = {
   headers: new HttpHeaders({
     'Content-Type':  'application/json',
     'Authorization': token
   })
 };
  console.log("before data");
  return this.http.put<any>('http://192.168.150.64:8080/myspace/queryforum/updateanswer',data,httpOptions);
 
}
/**
 * It gets info from answer component which answer to be deleted and sends that info to server and sends that info to subscribe method 
 * @param data 
 */
deleteAnswer(data)
{
  debugger;
  let id=localStorage.getItem("empId");
  let token=localStorage.getItem("jwt-token");
  const httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
      'Authorization': token
    })
  };
  return  this.http.delete<any>('http://192.168.150.64:8080//myspace/queryforum/deleteAnswer/'+id+"/"+data,httpOptions);   
}
}


