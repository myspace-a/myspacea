import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PostraveComponent } from './postrave.component';

describe('PostraveComponent', () => {
  let component: PostraveComponent;
  let fixture: ComponentFixture<PostraveComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostraveComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PostraveComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
