import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ServiceService } from '../service.service';

@Component({
  selector: 'app-postrave',
  templateUrl: './postrave.component.html',
  styleUrls: ['./postrave.component.css']
})
export class PostraveComponent implements OnInit {
/**
 * onSubmit Method is used to post Rave compliments for Employee Method
 */
  @ViewChild('f') loginform:NgForm;

  constructor(private service:ServiceService) { }

  ngOnInit() {
  }

  onSubmit()
  {

    let id = localStorage.getItem("empId");
    this.loginform.value.createdBy=id;
    this.service.PostRave(this.loginform.value).subscribe(
      (data)=>{
        if(data.statuscode == 200)
        {
       alert("Raveposted successfully");
        }
        
      }
    )
          
    
  }

}
